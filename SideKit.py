import numpy
import os
import sidekit
from sidekit import FeaturesExtractor
import logging
logging.basicConfig(level=logging.DEBUG)
if __name__ == '__main__':

    # Data source file for UBM
    maleDataSource = 'maleData'
    femaleDataSource = 'femaleData'

    maleList = []
    with open(maleDataSource, 'r') as f:
        for line in f:
            maleList.append(line.rstrip())
    femaleList = []
    with open(femaleDataSource, 'r') as f:
        for line in f:
            femaleList.append(line.rstrip())

    maleShowList = []
    # Create Showlist
    for item in maleList:
        voiceID = item[11:24]
        maleShowList.append(voiceID)

    femaleShowList = []
    # Create Showlist
    for item in femaleList:
        voiceID = item[11:24]
        femaleShowList.append(voiceID)

    # Create Channel List
    maleChannelList = []
    for item in maleList:
        maleChannelList.append(0)

    # Create Channel List
    femaleChannelList = []
    for item in femaleList:
        femaleChannelList.append(0)

    # Feature Extractor Object
    # extractor = FeaturesExtractor(audio_filename_structure=None,
    #                                       feature_filename_structure='feat/{}',
    #                                       sampling_frequency=16000,
    #                                       lower_frequency=200,
    #                                       higher_frequency=3800,
    #                                       filter_bank="log",
    #                                       filter_bank_size=24,
    #                                       window_size=0.025,
    #                                       shift=0.01,
    #                                       ceps_number=20,
    #                                       vad="snr",
    #                                       snr=40,
    #                                       pre_emphasis=0.97,
    #                                       save_param=["vad", "energy", "cep", "fb"],
    #                                       keep_all_features=True)
    #
    # extractor.save_list(audio_file_list=maleList, show_list=maleShowList, channel_list=maleChannelList)
    # extractor.save_list(audio_file_list=femaleList, show_list=femaleShowList, channel_list=femaleChannelList)
    # for data in show_list:
    #     # Delete feature if somehow already exist
    #     feature_folder = "feat/A001-Ahmad/"
    #     try:
    #         os.remove(feature_folder + data + ".h5")
    #     except OSError:
    #         pass
    #     extractor.save(data)

    # FeatureServer object
    server = sidekit.FeaturesServer(features_extractor=None,
                                    feature_filename_structure="Feat/{}",
                                    sources=None,
                                    dataset_list=["energy", "cep", "vad"],
                                    mask="[0-19]",
                                    feat_norm="cmvn",
                                    global_cmvn=None,
                                    dct_pca=False,
                                    dct_pca_config=None,
                                    sdc=False,
                                    sdc_config=None,
                                    delta=True,
                                    double_delta=True,
                                    delta_filter=None,
                                    context=None,
                                    traps_dct_nb=None,
                                    rasta=True,
                                    keep_all_features=True)

    # Open UBM From file
    ubmMale = sidekit.Mixture()
    ubmMale.read('maleUBM-Full.h5')

    ubmFemale = sidekit.Mixture()
    ubmFemale.read('femaleUBM-Full.h5')

    enroll_stat_female = sidekit.StatServer(statserver_file_name='stat_female.h5')
    print("FUCKING BIT")
    print("SHAPE STAT 0")

    print(enroll_stat_female.stat0.shape)

    print("SHAPE STAT 1")
    print(enroll_stat_female.stat1.shape)

    fa_male = sidekit.FactorAnalyser()

    fa_male.total_variability_single('stat_female.h5',
                                ubmFemale,
                                600,
                                nb_iter=20,
                                min_div=True,
                                tv_init=None,
                                batch_size=300,
                                save_init=False,
                                output_file_name='extractor_female.h5')
    # Create IdMap
    # male_idmap = sidekit.IdMap()
    # male_idmap.leftids = numpy.array(maleShowList)
    # male_idmap.rightids = numpy.array(maleShowList)
    # male_idmap.start = numpy.empty((maleShowList.__len__()), dtype="|O")
    # male_idmap.stop = numpy.empty((maleShowList.__len__()), dtype="|O")
    #
    # female_idmap = sidekit.IdMap()
    # female_idmap.leftids = numpy.array(femaleShowList)
    # female_idmap.rightids = numpy.array(femaleShowList)
    # female_idmap.start = numpy.empty((femaleShowList.__len__()), dtype="|O")
    # female_idmap.stop = numpy.empty((femaleShowList.__len__()), dtype="|O")
    #
    # print('Compute the sufficient statistics')

    # Create a StatServer for the enrollment data and compute the statistics
    # enroll_stat_male = sidekit.StatServer(male_idmap,
    #                                  distrib_nb=1024,
    #                                  feature_size=60)
    # enroll_stat_male.accumulate_stat(ubm=ubmMale,
    #                             feature_server=server,
    #                             seg_indices=range(enroll_stat_male.segset.shape[0]),
    #                             num_thread=1)
    # enroll_stat_male.write('stat_male.h5')

    # enroll_stat_female = sidekit.StatServer(female_idmap,
    #                                       distrib_nb=1024,
    #                                       feature_size=60)
    # enroll_stat_female.accumulate_stat(ubm=ubmFemale,
    #                                  feature_server=server,
    #                                  seg_indices=range(enroll_stat_female.segset.shape[0]),
    #                                  num_thread=1)
    # enroll_stat_female.write('stat_female.h5')

    # print("UBM TRAINING")
    # ubm = sidekit.Mixture()

    # ubm.EM_split(features_server=server,
    #              feature_list=femaleShowList,
    #              distrib_nb=1024,
    #              iterations=(1, 2, 2, 4, 4, 4, 4, 8, 8, 8, 8, 8, 8),
    #              num_thread=1,
    #              save_partial=False,
    #              ceil_cov=10,
    #              floor_cov=1e-2,
    #              )
    # ubm.write("femaleUBM.h5")
    #

    # Open UBM from files
    # Make male UBM full covariance
    # ubmMale = sidekit.Mixture()
    # ubmMale.read('maleUBM.h5')
    # ubmMaleFull = sidekit.Mixture()
    #
    # ubmMaleFull.EM_diag2full(ubmMale,features_server=server,featureList=maleShowList, iterations=1)
    # ubmMaleFull.write('maleUBM-Full.h5')

    # ubmFemale = sidekit.Mixture()
    # ubmFemale.read('femaleUBM.h5')
    # ubmFemaleFull = sidekit.Mixture()
    # ubmFemaleFull.EM_diag2full(ubmFemale, features_server=server, featureList=femaleShowList, iterations=1)
    # ubmFemaleFull.write('femaleUBM-Full.h5')
    # print("Creating IDMap")
    # idmap = sidekit.IdMap()
    # idmap.leftids = numpy.array(["model_1", "model_1", "model_1"])
    # idmap.rightids = numpy.array(["1", "2", "3"])
    # idmap.start = numpy.empty((3), dtype="|O")
    # idmap.stop = numpy.empty((3), dtype="|O")
    # print(idmap.validate())
    #
    # print("Creating NDX")
    # ndx = sidekit.Ndx()
    # ndx.modelset = numpy.array(["model_1"])
    # ndx.segset = numpy.array(["1", "2", "3"])
    # ndx.trialmask = numpy.ones((1, 3), dtype='bool')
    # print(ndx.validate())
    #
    # print("Creating Key")
    # key = sidekit.Key()
    # key.modelset = ndx.modelset
    # key.segset = ndx.segset
    # key.tar = numpy.ones((1, 3), dtype='bool')
    # key.non = numpy.zeros((1, 3), dtype='bool')
    # key.validate()
    #
    # print("Initialize Stat Server")
    # enroll_stat = sidekit.StatServer(idmap,
    #                                  distrib_nb=1024,
    #                                  feature_size=60)
    # print("Validate UBM")
    # print(ubm.validate())
    # print(ubm.get_distrib_nb())
    # print(ubm.dim())
    # enroll_stat.accumulate_stat(ubm=ubm,
    #                             feature_server=server,
    #                             seg_indices=range(enroll_stat.segset.shape[0]),
    #                             num_thread=1)
    # enroll_stat.write('data/stat_rsr2015_male_enroll.h5')